
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes, Route } from "react-router-dom";
export * from './home.jsx';
export * from './cart.jsx';
export * from './favorite.jsx';