import { addItemToFavorite, deleteItemFavorite } from "../actions/favorite";

const initialState = {
  favoriteItems:[]
}

export function favoriteReducer  (state = initialState, action)  {
  switch (action.type) {
    case 'ADD_FAVORITE':
      return {
        
        favoriteItems: addItemToFavorite(state.favoriteItems, action.payload)
        
      }
      case 'DELETE_FAVORITE':
  return {
    ...state,
    favoriteItems: deleteItemFavorite(state.favoriteItems, action.payload)
  }
      
    default:
      return state;
  }
}
