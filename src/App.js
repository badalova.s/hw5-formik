import logo from './logo.svg';
import './App.scss';
import Home from "./pages/home";
import Cart from "./pages/cart";
import Favorite from "./pages/favotire";
import Header from './header';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getListssAsync } from './redux/actions/lists';
// import { getCard } from './redux/actions/card';

function App() {
  const dispatch = useDispatch();
  const listProduct = useSelector((state) => state.lists);
  const cart=(useSelector((state) => state.carts.cartItems))
  const favorite=(useSelector((state) => state.favorite.favoriteItems))
  
  useEffect(() => {
    dispatch(getListssAsync());
  }, [dispatch]
)

  return (  


  <BrowserRouter>
  <Header curent={cart?cart.length:0} favorites={favorite?favorite.length:0}/>
    <Routes>
        <Route index element={<Home list={listProduct} cart={cart}/>} />
        <Route path="cart" element={<Cart cart={cart}  />} />
        <Route path="favotire" element={<Favorite favorite={favorite}  />} />
      
    </Routes>
    

  </BrowserRouter>
  
  )

}


export default App;
