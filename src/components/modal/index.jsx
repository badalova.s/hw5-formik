import { Component } from "react";
import "./modal.scss";
import List from "../list";
import { useSelector } from "react-redux";
import { toggleModal } from "../../redux/actions/modal";
import { useDispatch } from "react-redux";

export default function Modal({Click,active,add,question}) {
  const dispatch = useDispatch();
  const modal =useSelector((state)=>state.modal.modal)
       return (
        
        <div className={modal ? "modal active" : "modal"} onClick={()=>dispatch(toggleModal())}>
        
        <div className={modal ? "modal_content active" : "modal_content"} onClick={e=>e.stopPropagation()}>
        <div className="modal__exit" onClick={()=>dispatch(toggleModal())} >X</div>
        <div>{question}</div>
          
        
        <div className="modal_btn_conteiner">
        <button  onClick={add} className="modal_btn" >OK</button>
        </div>
        </div>
        </div>
      );
    
  }
  