import {useFormik, yupToFormErrors} from "formik"
import { buy } from "../../redux/actions/card"; 
import { connect } from 'react-redux';
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import * as Yup from "yup";
import "./form.scss"
const validationSchema= Yup.object({
    Name:Yup.string().min(2,'min 2 letters requred').max(20,'min 2 letters requred').required('Enter name'),
    Surname:Yup.string().min(2,'min 2 letters requred').max(20,'max 20 letters requred').required('Enter surname'),
    Age:Yup
    .number()
        .required('Enter age')
        .min(18, 'You must be 18 years old'),
    Mobile:Yup
        .number()
            .required('Enter mobile'),
    Address:Yup
    .string().required('Enter adress')



})
export default function Form (){
    const dispatch = useDispatch();
    const cart=(useSelector((state) => state.carts.cartItems))
    const formik =useFormik({
        initialValues:
        {
            Name:'',
            Surname:'',
            Age:'',
            Address:'',
            Mobile:''

        },
        validationSchema,
        onSubmit:(values)=>{
            console.log (JSON.stringify(values))
          
            console.log(JSON.stringify(cart))
            dispatch(buy())

            
        }
    })

    return (<form className="form" onSubmit={formik.handleSubmit}>
       
        <label htmlFor="Name">Name</label>
        <input  type="text" id="Name" name="Name" value={formik.values.Name} onChange={formik.handleChange}></input>
        {formik.errors.Name?(<p style={{color:"red"}}> {formik.errors.Name}</p>):
        ("")
        }
        <label htmlFor="Surname">Surname</label>
        <input type="text" id="Surname"name="Surname" value={formik.values.Surname} onChange={formik.handleChange}></input>
        {formik.errors.Surname?(<p style={{color:"red"}}> {formik.errors.Surname}</p>):
        ("")
        }
        <label htmlFor="Age">Age</label>
        <input type="text" id="Age" name="Age" value={formik.values.Age} onChange={formik.handleChange}></input>
        {formik.errors.Age?(<p style={{color:"red"}}> {formik.errors.Age}</p>):
        ("")
        }
        <label htmlFor="Address">Address</label>
        <input type="text" id="Address" name="Address" value={formik.values.Address} onChange={formik.handleChange}></input>
        {formik.errors.Address?(<p style={{color:"red"}}> {formik.errors.Address}</p>):
        ("")
        }
        <label htmlFor="Mobile">Mobile</label>
        <input type="text" id="Mobile" name="Mobile" value={formik.values.Mobile} onChange={formik.handleChange}></input>
        {formik.errors.Mobile?(<p style={{color:"red"}}> {formik.errors.Mobile}</p>):
        ("")
        }
        <button  type="submit">Checkout</button>
    </form>)
}
