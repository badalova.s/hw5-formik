import { Component } from "react";
import PropTypes from 'prop-types';
import Card from "../card"
import Modal from "../modal"
import "./list.scss"
import { useEffect, useState } from "react";
import ButtonAdd from "../button_add"
import ButtonFav from "../button_fav"
import {useDispatch} from "react-redux";
import {toggleModal} from "../../redux/actions/modal";
import { connect } from 'react-redux';
import { useSelector } from "react-redux";
import { addCart } from "../../redux/actions/card";
import { addFavorite } from "../../redux/actions/favorite";
import { deleteFavorite } from "../../redux/actions/favorite";



function List({cart,listproduct,addCart,deleteFavorite,addFavorite}) {
  
  let [its,setIt]=useState(null)
  const dispatch = useDispatch();

  if (listproduct==null) {
    return <div>loading...</div>
  } 
  else
  return (
    <>
   <div className="list">
      { listproduct.map((el)=> (
       <div className="list__card" key={el.id}>
          <Card key={el.id} name={el.name} image={el.image} 
            id={el.id}
            price={el.price}
            color={el.color}
            article={el.article}/>
            
           <div className="card__button">
           <ButtonAdd Click={()=>{
           dispatch(toggleModal())
           setIt(el) 
           }
           }/>
           <ButtonFav addDelete={(e)=>{
          if(e.target.parentNode.classList.contains('active')){
            e.target.parentNode.classList.remove('active')
            deleteFavorite(el.id)
        }else {
          e.target.parentNode.classList.add('active');
          addFavorite(el)
        }  
          }}
/>
                      </div>
                       </div>
                     ))}
                     
                      
                    
                    </div>

                    <Modal
          question={"Ви хочете додати цей товар до корзини"} 
          add={(e)=>{
            addCart(its) 
            dispatch(toggleModal())         
            }}/>          
            
                   </>

   
)
}
const mapDispatchToProps = dispatch => ({
  addCart: item => dispatch(addCart(item)),
  addFavorite: item => dispatch(addFavorite(item)),
  deleteFavorite: id => dispatch(deleteFavorite(id))
})
export default connect(null, mapDispatchToProps)(List)